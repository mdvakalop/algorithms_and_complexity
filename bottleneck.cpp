#include <stdio.h>
#include <list>
#include <array>
//#include <vector>
using namespace std;

#define BSIZE 1<<15
#define n 3000
#define m 100000
#define q 100000


class neighbAndWeight {		//this class is created for the neighb list
public:
	long int neighb;
	long long w;

	neighbAndWeight (long int a, long long int b) {
		neighb = a;
		w = b;
	}
	neighbAndWeight() {}

};

class edge {
public:
	long int u;
	long int v;
	long long int w;
	
	void init (long int a, long int b, long int c) {
		u = a;
		v = b;
		w = c;
	}
};

class trip {
public:
	long int u;
	long int v;

	void init (long int a, long int b) {
		u = a;
		v = b;
	}
};

trip trips[q]; 
bool visited[n];	//for bfs

array<list<neighbAndWeight>,n> neighbours;
//vector<list<neighbAndWeight>> neighbours(n);		//adj array of lists
edge edges[m]; 
edge MST[n-1];
long int parent[n];
long int setRank[n];
neighbAndWeight previ[n];

void quickSort(edge e[], long int low, long int high);
long int contentSet(long int x);
void unionSet(long int x, long int y);
 
int main () {
	long int i, c;
	long int N, M, Q;
	long int v1, v2;
	long long int w;
	long int MSTedges = 0;
	
	scanf("%li", &N);
	scanf("%li", &M);
	for (i=0; i<M; i++) {
		scanf("%li", &c);
		edges[i].u = c-1;		//for array-index
		scanf("%li", &c);
		edges[i].v = c-1;
		scanf("%lli", &(edges[i].w));
	}

	scanf("%li", &Q);
	i = 0;
	for (i=0; i<Q; i++) {
		scanf("%li", &c);
		trips[i].u = c-1;
		scanf("%li", &c);
		trips[i].v = c-1;
	}

	i = 0;
	for (i=0; i<N; i++) {
		parent[i] = i;	//a set for each node
		setRank[i] = 0;	//at the beginning all ranks are 0
	}

	quickSort(edges, 0, M-1);	//sort edges by weight (w) using quicksort

	i = 0;
	while (i < M && MSTedges <= N-1) {
		v1 = edges[i].u;
		v2 = edges[i].v;
		w = edges[i].w;
		if (contentSet(v1) != contentSet(v2)) {
			unionSet(v1, v2);
			MST[MSTedges] = edges[i];	//this edge belonges to MST
			neighbours.at(v1).push_back(neighbAndWeight(v2,w));
			neighbours.at(v2).push_back(neighbAndWeight(v1,w));
			MSTedges++;
		}
		i++;
	}
	//works by this point

	//from this point: search implementation BFS with list
	//first find the path, then count max edge in �(n)

	neighbAndWeight s, t, s0;
	
	i = 0;
	for (i=0; i<Q; i++) {		//for all trips asked
		list<neighbAndWeight> bfsQueue;
		long long int maxW = 0;
		bool found = false;
	
		for (int j=0; j<N; j++) 
			visited[j] = false;

		s.neighb = trips[i].u;	//s is a node with one neighb == u
		s.w = -1;
		t.neighb = trips[i].v;  //t is a node with one neighb == v
		t.w = 0;

		visited[s.neighb] = true;	//curent-starting node of trip is visited
		previ[s.neighb] = s;
		bfsQueue.push_back(s);
 
		list<neighbAndWeight>::iterator it;
 
		while(!bfsQueue.empty()) {
			// Dequeue front vertex from queue
			s0 = bfsQueue.front();
			bfsQueue.pop_front();
 
			// Get all adjacent vertices of the dequeued
			// vertex s0. If a adjacent has not been visited, 
			// then mark it visited and enqueue it
			for (it = neighbours.at(s0.neighb).begin(); it != neighbours.at(s0.neighb).end(); ++it) {
				if (found) break;
				long int neighb = it->neighb;
				if (!visited[neighb]) {
					visited[neighb] = true;
					previ[neighb].neighb = s0.neighb;
					previ[neighb].w = it->w;
					if (neighb == t.neighb)
						found = true;
					bfsQueue.push_back(*it);
				}
			}
			if (found) break;
		}

		neighbAndWeight ii = t;
		long long int currW;
		while (previ[ii.neighb].w != -1) {	//it is -1 when previ[ii.neighb] = s
			currW = previ[ii.neighb].w;
			if (currW > maxW) maxW = currW;
			ii = previ[ii.neighb];
		}
		printf("%lli\n", maxW);
	}
	return 0;
}


// A utility function to swap two elements
void swap(edge* a, edge* b) {
	edge t = *a;
	//printf("u=%li, v=%li, w=%lli\n", t.u, t.v, t.w);
    *a = *b;
    *b = t;
}
 
/* This function takes last element as pivot, places
   the pivot element at its correct position in sorted
    array, and places all smaller (smaller than pivot)
   to left of pivot and all greater elements to right
   of pivot */
long int partition (edge e[], long int low, long int high) {
    long long int pivot = e[high].w;    // pivot
    long int i = (low - 1);  // Index of smaller element
 
    for (long int j = low; j <= high- 1; j++) {
        // If current element is smaller than or
        // equal to pivot
        if (e[j].w <= pivot) {
            i++;    // increment index of smaller element
            swap(&e[i], &e[j]);
        }
    }
    swap(&e[i + 1], &e[high]);
    return (i + 1);
}
 
/* The main function that implements QuickSort
 arr[] --> Array to be sorted,
  low  --> Starting index,
  high  --> Ending index */
void quickSort(edge e[], long int low, long int high)
{
    if (low < high)
    {
        /* pi is partitioning index, arr[p] is now
           at right place */
        long int pi = partition(e, low, high);
 
        // Separately sort elements before
        // partition and after partition
        quickSort(e, low, pi - 1);
        quickSort(e, pi + 1, high);
    }
}



//This is the concept of path compression.
//So basically instead of having difference parents for different members of the same set
//we want one parent which will reduce the height of the tree(which we visualise(see Cormen))
//and improve our search efficiency.
long int contentSet(long int x) {
	if (x != parent[x])
		parent[x] = contentSet(parent[x]);
	return parent[x];
}


//This is the concept of union-by-setRank.
//The parent of the smaller set will be replaced by parent of bigger set.
//This in turn will lead to fewer operations when we call make_set next.
void link(long int x, long int y) {
	if (setRank[x] > setRank[y]) 
		parent[y]=x;
	else {
		parent[x] = y;
		if (setRank[x] == setRank[y]) 
			setRank[y]++;
	}
}

void unionSet(long int x, long int y) {
	link(contentSet(x), contentSet(y));
}

