#include <iostream>
#include <vector>
#include <tuple>
#include <fstream>
#include <stdio.h>
using namespace std;

//defines and initializations for readLong
#define BSIZE 1<<15
char buffer[BSIZE];
long bpos = 0L, bsize = 0L;

static vector <long long int> distL(500000), distR(500000), distU(500000), distD(500000), distFinal(500000);

class node {
public:
	long int val;
	long int position;
};


long readLong ();
void mergesort (vector <node> &arr, long int l, long int r);
void merge (vector <node> &arr, long int l, long int m, long int r);

int main (int argc, char **argv) {
	ifstream myfile;
	
	long int i, n, c;
	long long int minDist;
	myfile.open(argv[1]);
	//myfile >> n;
	n = readLong();
	vector <node> x, y;									//static
	node cur;

	for (i=0; i<n; i++) {
		//myfile >> c;
		c = readLong();
		cur.position = i;
		cur.val = c;
		x.push_back(cur);
		//myfile >> c;
		c = readLong();
		cur.val = c;
		y.push_back(cur);
	}
	mergesort(x, 0, n-1);
	mergesort(y, 0, n-1);

	for (i=0; i<n; i++) {  //distance from i from all the left elements of it(j<i) 
		if (i == 0) distL[i] = 0;
		else distL[i] = distL[i-1] + i*abs(x[i-1].val-x[i].val);
		if (i == 0) distD[i] = 0;		//same for down
		else distD[i] = distD[i-1] + i*abs(y[i-1].val-y[i].val);	
		if (i == 0) distR[n-1-i] = 0;	//same for right
		else distR[n-1-i] = distR[n-1-(i-1)] + i*abs(x[n-1-(i-1)].val-x[n-1-i].val);
		if (i == 0) distU[n-1-i] = 0;	//same for up
		else distU[n-1-i] = distU[n-1-(i-1)] + i*abs(y[n-1-(i-1)].val-y[n-1-i].val);
	}
	for (i=0; i<n; i++) {
		distFinal[x[i].position] += distR[i] + distL[i];
		distFinal[y[i].position] += distU[i] + distD[i];
	}
	for(i=0; i<n; i++) {
		if (i == 0) minDist = distFinal[i];
		else if (distFinal[i] < minDist) minDist = distFinal[i];
	}
	cout << minDist << endl;
	myfile.close();
	return 0;
}



//about mergsort and merge: code taken from GeeksForGeeks

void mergesort (vector<node> &arr, long int l, long int r) {
	if (l<r) {
		long int m = l + (r-l)/2;
		mergesort (arr, l, m);
		mergesort (arr, m+1, r);
		merge (arr, l, m, r);
	}
}

void merge (vector<node> &arr, long int l, long int m, long int r) {
	long int i, j, k;
    long int n1 = m - l + 1;
    long int n2 =  r - m;
	vector <node> Left(n1), Right(n2); 
    // Copy data to temp arrays Left[] and Right[] 
    for (i = 0; i < n1; i++)
        Left[i] = arr[l + i];
    for (j = 0; j < n2; j++)
        Right[j] = arr[m + 1 + j];
	// Merge the temp arrays back into arr[l..r]
    i = 0; // Initial index of first subarray
    j = 0; // Initial index of second subarray
    k = l; // Initial index of merged subarray
    while (i < n1 && j < n2) {
        if (Left[i].val <= Right[j].val) {
            arr[k] = Left[i];
            i++;
        }
        else {
            arr[k] = Right[j];
            j++;
        }
        k++;
    }
	// Copy the remaining elements of Left, if there are any 
    while (i < n1) {
        arr[k] = Left[i];
        i++;
        k++;
    }
	//same for Right
    while (j < n2) {
        arr[k] = Right[j];
        j++;
        k++;
    }
	//only one of these 2 whiles will run after the 1st while
}


long readLong() 
{
	long d = 0L, x = 0L;
	char c;
	while (1)  {
		if (bpos >= bsize) {
			bpos = 0;
			if (feof(stdin)) return x;
			bsize = fread(buffer, 1, BSIZE, stdin);
		}
		c = buffer[bpos++];
		if (c >= '0' && c <= '9') { x = x*10 + (c-'0'); d = 1; }
		else if (d == 1) return x;
	}
	return -1;
}
