#include <stdio.h>
#include <math.h>
#include <stdlib.h> 

#define BSIZE 1<<15
char buffer[BSIZE];
long bpos = 0L, bsize = 0L;


struct particle {
	//long int time;
	//long int veloc;
	long int i;
	double posit;
};

long int av[50000], bv[50000], at[50000], bt[50000];
double ax[50000], bx[50000];

long readLong();

int main(/*int argc, char **argv*/) {
	//FILE *mf;
	long int n, k, l, kcnt = 0; 
	long int i;
	double t, t0, t_dif;
	//long int av[4], bv[4], at[4], bt[4];
	//double ax[4], bx[4];
	
	//mf = fopen (argv[1], "r");
	n = readLong();
	l = readLong();
	k = readLong();
		
	for(i=0; i<n; i++){
		//a[i].i = i+1;
		at[i] = readLong();
		av[i] = readLong();
		ax[i] = 0;
	}
	for(i=0; i<n; i++){
		//b[i].i = i+1;
		bt[i] = readLong();
		bv[i] = readLong();
		bx[i] = 0;
	}
	
	//t= (1.0*l/2)/(a[0].veloc*1.0);
	t=l/2;
	

	struct particle maxA, maxB;
	//maxA = (struct particle*)malloc(sizeof(struct particle));
	//maxB = (struct particle*)malloc(sizeof(struct particle));
	maxA.posit = 0;
	maxB.posit = 0;
	int cr = 0;
	while (kcnt < k) {
		t0 = t;
		t_dif = t; //t_dif is what we add or sub in time during divide and conq in time
		while (1) {

			for (i=0; i<n; i++) {
				ax[i] = av[i] * (t0-at[i]);
				bx[i] = bv[i] * (t0-bt[i]);
			}

			for (i=0; i<n; i++)		//find a-particle that is more right of a's at t1 (max position x)
				if (ax[i] > maxA.posit) {
					//maxA = &a[i];
					maxA.posit = ax[i];
					maxA.i = i;
				}

			for (i=0; i<n; i++)		//find b-particle that is more left of b's at t1 (max position x)
				if (bx[i] > maxB.posit) {
					//maxB = &b[i];
					maxB.posit = bx[i];
					maxB.i = i;
				}

			if(maxA.posit + maxB.posit == l)  {
				printf ("%ld %ld\n", (maxA.i)+1, (maxB.i)+1);
				av[maxA.i] = 0;
				bv[maxB.i] = 0;
				maxA.posit = 0;
				maxB.posit = 0;
				kcnt++;
				break;
			}
			else if (maxA.posit + maxB.posit > l) {
				if (t_dif < 0.00001 && cr == 0) {
					printf ("%ld %ld\n", (maxA.i)+1, (maxB.i)+1);
					av[maxA.i] = 0;
					bv[maxB.i] = 0;
					maxA.posit = 0;
					maxB.posit = 0;
					kcnt++;
					break;
				}
				else {
					t0 -= t_dif/2.0;
					t_dif = t_dif/2.0;
					cr = 1;
					maxA.posit = 0;
					maxB.posit = 0;
				}
			}
			else if (maxA.posit + maxB.posit < l) {
				if (t_dif < 0.00001 && cr == 1){
					printf ("%ld %ld\n", (maxA.i)+1, (maxB.i)+1);
					av[maxA.i] = 0;
					bv[maxB.i] = 0;
					maxA.posit = 0;
					maxB.posit = 0;
					kcnt++;
					break;
				}
				else {
					t0 += t_dif/2.0;
					t_dif = t_dif/2.0;
					cr = 0;
					maxA.posit = 0;
					maxB.posit = 0;
				}
			}
		}
	}
	//fclose(mf);
	return 0; 
}


long readLong() {
	long d = 0L, x = 0L;
	char c;
	while (1)  {
		if (bpos >= bsize) {
			bpos = 0;
			if (feof(stdin)) return x;
			bsize = fread(buffer, 1, BSIZE, stdin);
		}
		c = buffer[bpos++];
		if (c >= '0' && c <= '9') { x = x*10 + (c-'0'); d = 1; }
		else if (d == 1) return x;
	}
	return -1;
}

