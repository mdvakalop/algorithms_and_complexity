    #include <stdio.h>  
      
    #define BSIZE 1<<15  
      
    char buffer[BSIZE];  
    long bpos = 0L, bsize = 0L;  
      
      
    long int dishes[1000000];  
    long long int sums[100000000], miam[1000000];  
      
    signed long readLong()   
    {  
        signed long d = 0L, x = 0L;  
        char c;  
      
        while (1)  {  
            if (bpos >= bsize) {  
                bpos = 0;  
                if (feof(stdin)) return x;  
                bsize = fread(buffer, 1, BSIZE, stdin);  
            }  
            c = buffer[bpos++];  
            if (c >= '0' && c <= '9') { x = x*10 + (c-'0'); d = 1; }  
            else if (d == 1) return x;  
        }  
        return -1;  
    }  
      
      
    int main () {  
        long int a, dish, i;  
        long long int N, plate;  
        long long int b, c, x_tone_plate=0, x=0;  
 
        scanf("%lli", &N);   
        scanf("%li", &a);  
        scanf("%lli", &b);  
        scanf("%lli", &c);  
        for (i=0; i<N; i++) {  
            scanf("%li", &(dishes[i]));    
            if (i==0) sums[i] = dishes[i];  
            else sums[i] = dishes[i] + sums[i-1];  
        }
  
        i = 0;  
        for (i=0; i<N; i++) {  
            miam[i] = a*sums[i]*sums[i] + b*sums[i] + c;  
            plate = 0;  
            if (i!=0) {  
                for (int k=i; k>=1; k--) {   
                    plate += dishes[k];  
                    x_tone_plate = a*plate*plate + b*plate + c;  
                    x = x_tone_plate + miam[k-1]; // miam[k-1] is best x_tone until first dish in "plate"  
                    if (x > miam[i]) miam[i] = x;  
                }  
            }  
        }  
        printf("%lli\n", miam[N-1]);  
      
        return 0;  
    }  



