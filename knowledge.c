    #include <limits.h>  
    #include <stdio.h>  
      
    #define BSIZE 1<<15  
      
    char buffer[BSIZE];  
    long bpos = 0L, bsize = 0L;  
      
    long int g[1001][100000], c[100000];  
      
    signed long readLong()   
    {  
        signed long d = 0L, x = 0L;  
        char c;  
      
        while (1)  {  
            if (bpos >= bsize) {  
                bpos = 0;  
                if (feof(stdin)) return x;  
                bsize = fread(buffer, 1, BSIZE, stdin);  
            }  
            c = buffer[bpos++];  
            if (c >= '0' && c <= '9') { x = x*10 + (c-'0'); d = 1; }  
            else if (d == 1) return x;  
        }  
        return -1;  
    }  
      
      
    int main () {  
        long int i;  
        long int tmp_g, prev, cur; //g[N+L] for simplier code  
        long int N;  
        int l, L;     

        N = readLong();  
        L = readLong();  
        for (i=0; i<N; i++)  
            c[i] = readLong();  
      
        for (i=0; i<=L; i++) // 1 village -> G=0 profit for any buy   
            g[i][0] = 0;  
        i=0;  
        for (i=0; i<N; i++) //0 buys -> G=0 profit  
            g[0][i] = 0;  
      
        i = 1;  
        l = 1;  
        for (l=1; l<=L; l++) {  
            prev = LONG_MIN;    //prev is max of diffs from (g[l][i-1]-c[i-1]) to (g[l-1][0]-c[0]). sell in i, 
				//from where is best to have bought  
                                //compares where is best to buy (village k from c[k]), computes the profit 
				//if you buy at k, profit so far -cost at k  
            i = 1;  
            for (i=1; i<N; i++) {  
                g[l][i] = g[l][i-1];  //init array of g: dont buy book at present village, best for previus villages  
                if (prev < g[l-1][i-1]-c[i-1]) prev = g[l-1][i-1]-c[i-1]; //update prev  
                cur = prev + c[i]; //selling here (includes buying in the best way because prev is best)  
                if (g[l][i] < cur) g[l][i] = cur;  
            }  
        }  
        printf("%li\n", g[L][N-1]);  
        //fclose(myfile);  
        return 0;  
    }  
